import pandas as pd
import plotly.express as px

df = pd.read_csv('summer2023-export.csv')


# Grouping bars together: https://stackabuse.com/plotly-bar-plot-tutorial-and-examples/
# fig.update_layout(barmode='group')

# good examples: https://www.geeksforgeeks.org/bar-chart-using-plotly-in-python/

# See # https://plotly.com/python/bar-charts/ for bar example
for column in df.columns:
    fig = px.bar(df, x=column, title=f'Bar Chart for {column}')
    fig.show()
