# Four day workweek experiment report

This report is available in markdown and LaTeX formats. Markdown is better for on-line browsing while LaTeX generates
PDF that is probably better for off-line reading.

- [Markdown](markdown/summer-report.md)
- [LaTeX (PDF)](latex/summer-report.pdf)

# Raw data

Raw data is stored in [data/](data/) directory in various formats: CSV, OpenOffice.

# Data collection

Data was extracted from gitlab using [git-snitch](https://gitlab.isc.org/tomek/git-snitch). Commands used:

`python ./git-snitch.py -t <secret-token> --since 2023-05-15 --until 2023-10-09 -f csv -u andrei,fdupont,marcin,mgodzina,piotrek,razvan,slawek,wlodek,tmark,tomek -p week activities`

`python ./git-snitch.py --format csv -u andrei,fdupont,marcin,mgodzina,piotrek,razvan,slawek,wlodek,tmark,tomek,tomasz --since 2023-05-15 --until 2023-10-15 -p week commits-periods``

# Python charts

Charts were generated using python. Python code requires two libs: pandas and plotly. The usual installation procedure applies:

```
python -m venv venv
source venv/bin/activate
pip install pandas plotly
```

Then run the script:

`python plot.py`

# Visualising data

Some experiments were made in jupyter lab.

Setup: `pip install jupyterlab pandas plotly`

Starting jupyter: `jupyter lab`, the open *.ipynb files from `python/` dir.
