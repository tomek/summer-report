
# Four Day Workweek Summer Experiment Report

Tomek Mrugalski, Ph.D.<br/>
Director of DHCP Engineering<br/>
tomek(at)isc(dot)org


## Abstract
During the summer of '23 ISC experimented with a four day workweek. The experiment was conducted by the whole staff of
the company. However, each department implemented it in a slightly different way. This report pertains to the DHCP
Engineering group, a software engineering team responsible for the development of an open-source software, with associated
commercial support. This paper describes the experiment, its goals, methodology and results.

Keywords: four day, experiment, workweek, efficiency, software development

## 1. Introduction

This document is an attempt to assess results of an experiment conducted during summer of 2023. During eleven weeks,
starting in mid-June and ending in early September, the software engineering team worked 32 hour weeks. This was
implemented as non-working Fridays. The team develops a small number of long term software projects. The major
project (Kea) has monthly development releases and one major, stable release per year. Another project (Stork)
has a development release every two months.

## 2. Metrics
Software development is a complex process and there is no single metric that can be used to measure overall efficiency.
To provide deeper insight into the experiment, many metrics were obtained. They can be broadly categorized
into objective and subjective. Objective metrics are those that can be mostly measured automatically, such as the number
of lines of code written, number of bugs closed etc. Subjective metrics are those that require human input and describe how
people feel about the experiment. The objective metrics were obtained by analyzing the data from GIT repositories and
other Gitlab interfaces. The subjective metrics were obtained by conducting a set of 3 surveys (before, during, and
after the experiment).

For better understanding of the experiment, the measurements took place over 22 weeks (5 weeks before, 11 weeks of the
experiment and 6 weeks afterwards). A comparative analysis from the previous year (2022) was also obtained. The
temporal resolution was chosen to be one week for objective metrics.

### 2.1 Objective Metrics
The following metrics were obtained. The team consists of 10 people. While there are three major projects (Kea, Stork
and ISC-DHCP), there are 14 repositories in total that cover the base code for each project, test tools, migration
assistant, packages, Docker images, wiki, hooks etc.

**commits** -- number of commits pushed. This covers pushes to all branches, including master, release and
development branches. Some developers prefer a smaller number of large commits, while others prefer the opposite. This
metric is a good high level indicator of actual code changes, but is poor regarding other activities, e.g. research,
solving customer's problems with configuration changes etc. This was obtained by analyzing the git history for each commit.

**diff** -- number of lines changed in the code. This is somewhat better than \emph{commits}, as it shows the
actual amount of work done. It is not perfect in the sense that some changes are trivial, e.g. changing a comment,
adding or removing an empty line still counts as a line changed. However, these small edits are the minority and also
they improve the code quality, so they should not be ignored.

**files** -- number of file changes. Due to the way this metric was calculated, it is not very useful for the
context of this experiment. For each commit, it generated a list of files changed and then summed the number of files for
all commits in a given period. If there was a single file edited 10 times, \emph{files} metric would show 10.

**pto** -- cumulative number of days off. This metric had to be calculated manually. The HR system that ISC uses does
not provide an easy way to export the data automatically. This metric was obtained by summing the number of national
holidays, PTO (paid time off) and sick days for each staff person and then aggregating for the whole team.

**opened** -- number of tickets opened. This is a very good high level indication of how active the team was. This
covers all the tickets the members of the team opened, but does not cover tickets opened by external (customers, support
team, users) parties. It's a good indication of the team's activity, but not necessarily of the project's well being.

**closed** -- number of tickets closed. Typically, this metric is used to measure the efficiency of the team, as
it's the closest approximation to the number of problems solved. It does not cover all the cases (such as addressing
customer problems without opening any tickets, or responding to mailing list posts), but it's a good approximation.

**comments** -- having a discussion is an essential part of the software development process. Most tickets have an
initial discussion, in particular when the reporter is an external user or customer. For issues that are not
straight-forward, there is often a discussion about the proposed solution before it is implemented. Finally, each ticket
goes through a review that in great majority of cases results in a discussion about the proposed changes. This metric
is a very good approximation of the amount of the non-coding work done.

**updated** -- number of entities (tickets, milestones, wiki pages, merge requests) updated. Each entity has a
description that is sometimes updated, as mistakes are spotted or extra information becomes available. This metric
is a reasonably good approximation of how attentive the team is. A good engineer is expected to pay attention to
details.

**new-branches** -- number of new branches. The coding practice in the team is to create a new branch for each new
problem or feature being tackled. In most cases, there is one branch per ticket, however more complex tickets often have
multiple branches. Pushing to a new branch effectively means a start of solution phase of a problem. Figuratively
speaking, this metric approximates how creative the team was or how eager it was to tackle new problems.

**approved** -- number of merge requests (MR) approved by a reviewer. This metric is a good approximation of the
reviewing activity.

**merged** -- number of merge requests merged. This metric is a good approximation of the amount of work completed
successfully. Typically, each merge implies a resolution of an issue, except most complex tickets that sometimes require
multiple MRs.

**total** -- this is a sum of all activities as reported by Gitlab. It includes all the activities (opened,
closed, comments, updated, new-branches, approved, merged) and several additional ones that are infrequent enough that
are not reported separately, such as deleting spam tickets or administratively deleting branches that were determined
unsuitable. This metric is a good high level approximation of the overall activity of the team.

### 2.2 Subjective Metrics

Subjective metrics attempt to quantify how the team feels about the experiment. Even though the participants were
requested to express their opinions in a numerical scale from 1 to 10, the results are still subjective. However, this
is the only method currently available. Participants were requested to answer three surveys: before the experiment,
during, and after the experiment concluded. When possible, the questions were identical, so that the results could be
compared. The survey was strictly anonymous. To maintain objectivity, the author of this report abstained from
participating in the survey.

**work-life-balance** - The question was phrased as follows: _How satisfied are you with your work--life balance?_.
This question attempts to answer whether the job dominates over personal life. This metric is considered one of the major
non-financial aspects that determines long term satisfaction from a job. People tend to look for a new job if the current
job is too demanding in terms of hours dedicated per week.

**exhaustion** - The question was phrased as follows: _How exhausted do you feel be the end of the working
week?_. This question attempts to answer how overloaded the person feels. Extended periods of overwork are known to cause
burnouts, depression and often result in people leaving the company. This metric is one of key factors in the long term
staff retention.

**satisfaction** - The question was phrased as follows: _On average how is your work satisfaction? Did you like
your job, the amount and kind of tasks you expected to do, the environment you worked in? How satisfied are you with your
work?_. This question was broader than the parameters of the experiment, as the kind of tasks and the environment
didn't change during the experiment. However, the answers might possibly give some insight how the shorter working hours
could improve general perception of the job.

**compensation** - The question was phrased as follows: _Do you feel you were compensated for your job
adequately, including your salary, benefits, available PTO, sick days, health care, flexible work time, provided
hardware, etc?_. During the experiment, the salary hasn't changed, although if monthly salary was converted to per hour
basis, it could be perceived as a temporary pay raise. The other benefits were not changed either, except the one extra
free day per week.

**perceived-success** - The question was phrased as follows: _In your opinion, was the summer experiment a success?_.
This question was asked only in the post-experiment survey.

**productive** - The question was phased as follows: _Do you feel that you were less or more productive during
the experiment?_. This question was asked only in the post-experiment survey.

There were also two additional non-quantitative questions: _If it was completely up to you, how would you change the
experiment if we were to repeat it some time in the future?_ and general free text field for any additional comments.
Those will be discussed in the later section of this report.

## 3. Results

## 3.1 Objective metrics

The data was aggregated on weekly basis. Each week was given an identifier to easier recognize its temporal location.
For example, May 3 is the third week of May. A week starts on Monday. In case of a week crossing a months boundary, the
Monday date determines which month the week belongs to. The 2023 data covers five weeks before the expriment

The following table and the following charts present the same information, just in textual and graphical forms.

| **week** | **from** | **commits** | **files changed** | **lines added** | **lines deleted** | **experiment** | **PTO** | **total** | **accepted** | **approved** | **closed** | **comments** | **opened** | **new-branches** | **pushed_to** | **updated** |
|----------|----------|-------------|-------------------|-----------------|-------------------|----------------|---------|-----------|--------------|--------------|------------|--------------|------------|------------------|---------------|-------------|
| May3     | 23-05-15 | 99          | 824               | 7071            | 61624             | 0              | 9       | 371       | 15           | 7            | 25         | 116          | 54         | 25               | 125           | 0           |
| May4     | 23-05-22 | 169         | 482               | 11407           | 7634              | 0              | 4       | 705       | 31           | 18           | 29         | 266          | 52         | 38               | 267           | 0           |
| May5     | 23-05-29 | 127         | 883               | 19277           | 26108             | 0              | 8       | 483       | 22           | 16           | 28         | 167          | 43         | 26               | 159           | 14          |
| Jun1     | 23-06-05 | 129         | 523               | 6317            | 3526              | 0              | 5       | 433       | 11           | 8            | 13         | 187          | 34         | 21               | 142           | 7           |
| Jun2     | 23-06-12 | 108         | 372               | 9938            | 3435              | 0              | 10      | 384       | 10           | 5            | 20         | 167          | 36         | 25               | 119           | 0           |
| Jun3     | 23-06-19 | 116         | 431               | 7465            | 2373              | 1              | 8       | 546       | 26           | 18           | 22         | 238          | 30         | 20               | 186           | 5           |
| Jun4     | 23-06-26 | 159         | 492               | 7039            | 3854              | 1              | 3       | 722       | 43           | 25           | 25         | 285          | 55         | 42               | 232           | 10          |
| Jul1     | 23-07-03 | 122         | 358               | 31113           | 6488              | 1              | 1       | 341       | 10           | 9            | 15         | 105          | 39         | 25               | 128           | 7           |
| Jul2     | 23-07-10 | 63          | 174               | 5418            | 1447              | 1              | 13      | 232       | 14           | 9            | 9          | 64           | 24         | 15               | 90            | 0           |
| Jul3     | 23-07-17 | 91          | 287               | 2871            | 1252              | 1              | 13      | 432       | 29           | 20           | 25         | 137          | 37         | 22               | 154           | 3           |
| Jul4     | 23-07-24 | 79          | 305               | 7789            | 5162              | 1              | 12      | 288       | 7            | 7            | 9          | 118          | 12         | 13               | 117           | 0           |
| Jul5     | 23-07-31 | 81          | 183               | 1768            | 1154              | 1              | 0       | 476       | 22           | 7            | 34         | 189          | 64         | 26               | 113           | 11          |
| Aug1     | 23-08-07 | 87          | 289               | 3925            | 1589              | 1              | 17      | 396       | 19           | 7            | 25         | 102          | 37         | 19               | 174           | 10          |
| Aug2     | 23-08-14 | 36          | 192               | 4504            | 246               | 1              | 14      | 167       | 8            | 3            | 7          | 62           | 13         | 10               | 52            | 7           |
| Aug3     | 23-08-21 | 48          | 357               | 8844            | 7803              | 1              | 8       | 268       | 5            | 4            | 13         | 138          | 24         | 9                | 68            | 4           |
| Aug4     | 23-08-28 | 71          | 219               | 6333            | 1217              | 1              | 5       | 311       | 10           | 9            | 14         | 128          | 35         | 15               | 90            | 7           |
| Sep1     | 23-09-04 | 168         | 564               | 27969           | 63087             | 0              | 1       | 437       | 9            | 7            | 17         | 174          | 38         | 27               | 145           | 15          |
| Sep2     | 23-09-11 | 163         | 555               | 62969           | 66659             | 0              | 0       | 666       | 20           | 14           | 20         | 313          | 46         | 30               | 204           | 1           |
| Sep3     | 23-09-18 | 220         | 459               | 19544           | 4819              | 0              | 1       | 705       | 24           | 15           | 33         | 252          | 70         | 43               | 247           | 13          |
| Sep4     | 23-09-25 | 199         | 446               | 3577            | 1816              | 0              | 4       | 834       | 21           | 22           | 26         | 458          | 46         | 28               | 220           | 5           |
| Oct1     | 23-10-02 | 180         | 397               | 16523           | 3852              | 0              | 3       | 678       | 26           | 15           | 33         | 319          | 44         | 23               | 197           | 11          |
| Oct2     | 23-10-09 | 108         | 345               | 6656            | 2561              | 0              | 2       | 597       | 20           | 13           | 24         | 334          | 41         | 21               | 112           | 20          |

The following table and two charts presents historical data for 2022, as a reference data. There was no experiment
conducted in 2022.

| **week** | **from** | **commits** | **files changed** | **lines added** | **lines deleted** | **experiment** | **PTO** | **total** | **accepted** | **approved** | **closed** | **comments** | **opened** | **new-branches** | **pushed_to** | **updated** |
|----------|----------|-------------|-------------------|-----------------|-------------------|----------------|---------|-----------|--------------|--------------|------------|--------------|------------|------------------|---------------|-------------|
| May2     | 22-05-09 | 111         | 332               | 12096           | 1718              | 0              | 0       | 345       | 8            | 4            | 22         | 121          | 44         | 20               | 110           | 15          |
| May3     | 22-05-16 | 179         | 396               | 8012            | 5420              | 0              | 2       | 593       | 23           | 14           | 22         | 254          | 34         | 25               | 206           | 7           |
| May4     | 22-05-23 | 31          | 143               | 1019            | 516               | 0              | 3       | 195       | 6            | 3            | 11         | 65           | 40         | 11               | 47            | 7           |
| May5     | 22-05-30 | 112         | 290               | 5746            | 1859              | 0              | 11      | 415       | 18           | 9            | 20         | 184          | 28         | 16               | 126           | 8           |
| Jun1     | 22-06-06 | 88          | 293               | 14813           | 4918              | 0              | "3,5"   | 341       | 10           | 6            | 14         | 115          | 30         | 13               | 151           | 2           |
| Jun2     | 22-06-13 | 81          | 251               | 8111            | 1231              | 0              | 5       | 326       | 9            | 4            | 6          | 166          | 24         | 17               | 95            | 2           |
| Jun3     | 22-06-20 | 104         | 353               | 10698           | 1857              | 0              | 1       | 649       | 26           | 18           | 29         | 354          | 39         | 23               | 158           | 0           |
| Jun4     | 22-06-27 | 106         | 295               | 4898            | 3162              | 0              | "8,5"   | 526       | 19           | 13           | 24         | 241          | 46         | 27               | 148           | 7           |
| Jul1     | 22-07-04 | 107         | 283               | 9408            | 2662              | 0              | 6       | 382       | 9            | 4            | 19         | 176          | 38         | 16               | 117           | 1           |
| Jul2     | 22-07-11 | 67          | 133               | 3107            | 1008              | 0              | 12      | 328       | 11           | 7            | 16         | 152          | 34         | 19               | 73            | 8           |
| Jul3     | 22-07-18 | 129         | 379               | 8717            | 2071              | 0              | 6       | 640       | 35           | 16           | 36         | 264          | 49         | 37               | 182           | 19          |
| Jul4     | 22-07-25 | 87          | 905               | 6980            | 3680              | 0              | "15,5"  | 411       | 17           | 10           | 22         | 122          | 43         | 27               | 147           | 22          |
| Aug1     | 22-08-01 | 80          | 188               | 5372            | 1064              | 0              | 7       | 380       | 14           | 8            | 13         | 163          | 30         | 17               | 130           | 1           |
| Aug2     | 22-08-08 | 71          | 199               | 11652           | 2850              | 0              | 6       | 301       | 10           | 8            | 8          | 122          | 29         | 16               | 101           | 3           |
| Aug3     | 22-08-15 | 49          | 243               | 4094            | 1023              | 0              | "20,5"  | 244       | 7            | 3            | 7          | 137          | 19         | 9                | 56            | 6           |
| Aug4     | 22-08-22 | 93          | 254               | 4986            | 1684              | 0              | "9,5"   | 353       | 19           | 10           | 14         | 144          | 17         | 12               | 134           | 2           |
| Aug5     | 22-08-29 | 140         | 307               | 7341            | 2132              | 0              | "5,5"   | 546       | 20           | 8            | 23         | 161          | 35         | 22               | 213           | 55          |
| Sep1     | 22-09-05 | 100         | 250               | 4912            | 1307              | 0              | "4,5"   | 340       | 12           | 5            | 13         | 106          | 40         | 25               | 109           | 20          |
| Sep2     | 22-09-12 | 94          | 259               | 9202            | 1565              | 0              | 4       | 293       | 10           | 9            | 3          | 77           | 26         | 11               | 141           | 14          |
| Sep3     | 22-09-19 | 78          | 226               | 12599           | 8517              | 0              | "12,5"  | 439       | 15           | 11           | 17         | 227          | 18         | 11               | 126           | 12          |
| Sep4     | 22-09-26 | 58          | 215               | 3859            | 3525              | 0              | 16      | 244       | 12           | 3            | 8          | 96           | 29         | 17               | 74            | 3           |
| Oct1     | 22-10-03 | 94          | 307               | 9111            | 4343              | 0              | 9       | 244       | 12           | 3            | 8          | 96           | 29         | 17               | 74            | 3           |
| Oct2     | 22-10-10 | 78          | 318               | 14209           | 6045              | 0              | "1,5"   | 430       | 20           | 18           | 15         | 193          | 26         | 13               | 139           | 4           |

The following two charts show the number of lines changed for 2023 and for 2022 as a comparative data.
Both charts have their y axes set to 80.000, so easier visual comparison is possible. This scale allows better
visual comparison, although at the price of some values being truncated. There were two weeks immediately after the
experiment concluded that exceeded this range (91k and 129k lines). The values as shown on the chart are truncated.

![GIT activity 2023](../img/diff23.png)

![GIT activity 2022](../img/diff22.png)

The following two charts show various Gitlab activities.

![Objective metrics 2023](../img/gitlab2023.png)

![Objective metrics 2022](../img/gitlab2022.png)

### 3.2 Subjective: Work - life balance

The question was _How satisfied are you with your work--life balance?_

The answer was rated from 1 (miserable) to 10 (absolutely great).

![Work-life balance (before)](../img/work-life-balance1.png)
![Work-life balance (during)](../img/work-life-balance2.png)
![Work-life balance (after)](../img/work-life-balance3.png)

### 3.3 Subjective: Satisfaction

The question was _On average how is your work satisfaction? Did you like your job, the amount and kind of tasks you
expected to do, the environment you worked in? How satisfied are you with your work?_

The answer was rated from 1 (hate my useless job) to 10 (love my job and
could work here until retirement)

![Satisfaction (before)](../img/satisfaction1.png)
![Satisfaction (during)](../img/satisfaction2.png)
![Satisfaction (after)](../img/satisfaction3.png)

### 3.4 Subjective: Exhaustion
The question was _How exhausted do you feel be the end of the working week?_

The answer was rated from 1 (not at all, I could go on) to 10 (completely
out of brain powers, couldn't do anything).

![Exhaustion (before)](../img/exhaustion1.png)
![Exhaustion (during)](../img/exhaustion2.png)
![Exhaustion (after)](../img/exhaustion3.png)

### 3.5 Subjective: Compensation

The question was _Do you feel you were compensated for your job adequately, including your salary, benefits, available
PTO, sick days, health care, flexible work time, provided hardware, etc?_

The answer was rated from 1 (totally not satisfied) to 10 (satisfied completely).

![Compensation (before)](../img/compensation1.png)
![Compensation (during)](../img/compensation2.png)
![Compensation (after)](../img/compensation3.png)

### 3.6 Survey feedback during experiment

Comment 1: _Creative work, like programming and QA requires some inspiration, and it is not always there. Reducing to 4
days enables to "compress" it more, so it can improve productivity in a week period. One loose day for some errands like
car mechanic etc allows to reduce some unplanned and not convinient absence._

Comment 2: _I love it! ISC is great to be able to provide this kind of experience!_

Comment 3: _The free Fridays are an excellent idea. They allowed me to do my homework, learn new things, spend some time on my hobbies, and relax, all in a week. Previously, I needed to live in a hurry or decide what to ignore (or sleep less...)._

_Regarding my work performance, I see I'm much less tired at the end of the week. Previously, on Friday, I could not_
_wait for the day ends. Now, I'm on fire all week, and I'm a bit surprised at the end of Thursday that I need to back to_
_work only on Monday. I no longer count the days to a weekend. I can't rate reliable if I do now fewer tasks per week_
_than previously because it depends on the kind of tasks. But I noticed I'm more enthusiastic about taking on a new_
_complex task after finishing another one during the same week. Previously I needed to do some easier tickets to recover,_
_and I preferred to start any complex ones from a new week._ _The free Friday also increases the ISC's position as an_
_excellent employer. It is a benefit that strongly distinguishes_ _our company from others. In my humble opinion, the_
_additional free days are more valuable than the equivalent salary increase._

Comment 4: _I used this period to get more time off. It's more convenient to spend 4 days of PTO to get the entire week off than to spend 5 days of PTO, if that makes sense. This might not appear relevant to the experiment, but maybe it is._

Comment 5: _I was able to do things that so far were "not important enough to take PTO" and also "not important enough to change my day schedule" 3 day weekend also allowed me to take longer PTOs and rest after 2.4.0 marathon. Also I without "let's put that on Friday" option I am making progress with small tasks over the week_

Comment 6: _Having a three day weekend is quite nice.  It makes it a lot easier to take care of necessities while leaving time for R\&R,  relieving a surprising amount of stress.   I have to admit though, to feeling guilty not working five days, perhaps because its  so in-grained by my life. I couldn't help feeling like I wasn't earning my salary.  It's hard to gauge whether it made us more productive or not.  It doesn't seem as though we lost any ground.  Overall I am enjoying it and it has improved my overall well-being._

Comment 7: _I love the experiment. I have the impression that work that had to be done was done on time and work-life balance is better during summer season with 4 working days per week._

Comment 8: _4 days working week is great for maintaining work-life balance. However, another interesting experiment would be a 6 hours long working day for 5 days a week. Sometimes, Friday off doesn't fit into the schedule or weather. A shorter working day should give a bit more flexibility throughout the whole week. Just before the major release, I felt like I lacked the working Friday. Having a 6 hours long Friday would give a chance to address some outstanding issues. Just a thought. Overall, 4-days long week is great._

**Changes suggested by participants**

The exact question was _if it was up to you, how would you change the experiment if we were to repeat it?_.

Comment 1: _move around so it is always summer?:))_

Comment 2: _For my dev team I would not change anything, I think all was ok._

Comment 3: _I would run such experiment not in the summer time, when productivity is generally lower_
_This experiment was highly influenced by Kea 2.4.0 Release, and vacations._
_It would be wise to repeat it in Spring or Fall when there is normal work flow to see the difference._

Comment 4: _We could move the Kea call from Thursday to Wednesday or Tuesday. It would give us more time to check the_
_post-meeting conclusions with fresh memories. During the experiment, we had only few hours to do it before the 3-day_
_break, so we could forget about some nuances to Monday._

### 3.6 Survey feedback after experiment

The following comments were submitted for the free text feedback:

Comment 1: _it is really nice to have 4 days work week…it would be a stretch to have this all year long?_

Comment 2: _I loved the experiment. I could use the Fridays off to spend more time with my family, plan 3 day weekend
trips etc. I hope it could happen again next summer :)_

Comment 3: _At first I thought that productivity was much lower, but when I adjusted to the entire week plan I got a lot_
_things done. And still had long weekend to enjoy time off. Over all it was cool experiment, and when weather was bad on_
_Friday - I was just learning python and c++._

Comment 4: _Vacations are great time to cut on work hours, so they can cope with heat waves, weekend family trips etc._
_In such unproductive time of year in software development, cutting work week probably will not influence productivity,_
_if not even enhance it. But it will certainly boost morale._

Comment 5: _I'm happy that I could join the experiment. It was a perfect occasion to enjoy the summer, probably the_
_first as good since I graduated from school._

Comment 6: _I noticed my focus was distributed differently over the week than usual.   It was more regular. I was not_
_sad that the weekend is over on Mondays, and I was not exhausted on Thursdays. However, I was less eager to start the_
_new tasks on Thursdays than on Fridays in a 5-day workweek because I knew I could forget my concepts until Monday. My_
_fault was planning almost only big tasks for summer. It would be beneficial to have some small ones (for 1-2 days) for_
_cases when I finish the current job in the middle of the week._

Comment 7: _I didn't notice any performance degradation caused by less time to communicate with the rest of the team. On_
_the contrary, I thought people were more likely to reply out of their standard working hours._

## 4. Discussion

The following section covers random thoughts and comments.

With the Kea project being more mature and the code base exceeding 1MLOC (million lines of code), there's a desire to
avoid growing the code base in terms of number of lines, unless there's a good reason for it. Often times, the code can be
refactored to be more compact, which results in better, more maintainable code. On the flip side, Kea is heavily tested
and the test code is also counted as LOC. There is no expectation that more LOC means better.

Kea is a mature project with a lot of complex features. Simple tickets that require obvious code changes are becoming
less common. Instead, there are more and more tickets that require initial research and investigation. Often times, a
solution to a reported problem is found without any code changes necessary. This kind of work is also very useful and
should not be neglected when considering team performance. Note both *diff* and *commits* metrics would show zero for this activity.

The selection of the time (summer) was an excellent choice, although it was less convenient for people living in the
Southern hemisphere. However, since in the whole company northern hemisphere inhabitants outnumber sourthern ones 35:1,
it was the right choice.

Taking 4 days off to get a full week off was very popular.

One member of the team had a child during the experiment and took 3 weeks off. Once returned, he was often distracted.

One of the staff is retired and works part-time. His PTO was scaled up to represent full time.

An interesting historic context was brought up in one discussion. Six day workweek was common throughout the world in
early 20th century. US started the transition in the 1920s (for example Henry Ford introduced non-working Saturday in
1926) and effectively adopted five working week in 1938 (see 1938 Fair Labor Standards Act). Other countries did similar
transition at various times. For example, in Poland all Saturdays were working days in Poland up until 1972. The first
non-working Saturday was introduced in 1973, with initially only two per year. Over time, the ratio of
working/non-working Saturdays gradually increased, and eventually turned into all non-working Saturdays in the
mid-1980s. As the society matures and becomes more developed, the non-working Fridays experiment might be considered a
next step in this long term social evolution.

## 5. Conclusions

There were no escalations, complaints about neglected issues or any other form of negative feedback from customers
during the experiment. From that perspective, the experiment has no impact.

There was a major Kea release (2.4.0) on the first week of the experiment, which is typically the most intense period of the
year. Once the release is out, work intensity decreases for a while, so the team can recover. This mostly coincided with
the beginning of the experiment.

While there was some performance drop during the experiment in terms of team activity (number of opened and closed
tickets, comments, commits), the period after the experiment concluded yielded a significant increase, greatly above the
pre-experiment average. It is too early to say how long lasting that effect will be. The most likely explanation for
this phenomena is that after vacations and a relaxed summer the staff returned well-rested and motivated.

The subjective feedback from the staff was overwhelmingly positive.

The experiment can be considered an extra benefit, similar to bonuses, salary increase or health benefit.

From the company finances perspective, this kind of experiment is an attractive alternative to bonuses or pay raises, as
it did not incur any extra expenses, but it achieved an effect somewhat similar to salary increase. One interpretation
is the staff was paid more for the work conducted during the experiment.

The society went through a COVID pandemic in recent years. While the company survived in an excellent shape with no
layoffs and no significant decrease of business, the mental health of the staff was visibly affected. People were more
pessimistic, anxious, and eager to argue about petty technical details. The relaxed summer was an excellent way to
recover and return to more cheerful, pre-pandemic mood.

Isolating the experiment impact on performance is a difficult problem. An attempt was made to compare the results to
prior year's summer, but there were great many variables. As such, it was not a very well controlled environment. One
staff left the company and another was hired. The new hire is a much better performer, but is still ramping up. One staff
got their first baby, took three weeks off and when returned was frequently distracted.

Staff often took advantage of the four days week by taking four days off and getting the whole week off. This resulted in
the team on average using more PTO (251 man days off in 2023) compared to 168 man days off in 2022 in comparable period.
This is a positive side effect as the company in general has a problem with staff not taking time off. There are
significantly more people who are accrued close to maximum PTO and very few who tend to spend most of their PTO. This
tendency to actually use available PTO is beneficial.

## 6. Recommendations

Since no meaningful negative effects and many positive results were observed, the obvious recommendation is to repeat
similar kind of experiments in the future.

Impact on other teams is outside of scope of this analysis. If the impact on the company is deemed too significant in
the summer, an alternative to consider would be spreading the extra days off across the year, e.g. free each last Friday
of the month. However, from the discussions with the team it is apparent that the best effect is obtained by
accumulating the days off closely together.

Our team did not encounter any issues with the whole team being unavailable for each Friday. However, other teams may
experience some issues, e.g. a customer may possibly request a meeting on Friday. If such a problem appears, the next
experiment could alternate the off days between Monday and Friday. Splitting the days between parts of the team, e.g.
one part takes Fridays while another takes Mondays, is not recommended, as this would mean that there are only three
days where the full team could gather for meetings and have a fast paced discussion.

On behalf the team, author of this report would like to express our deepest gratitude for such a great way to make our
already excellent working conditions even better.

## 7. Bibliography

1. T. Mrugalski, _git-snitch_, software, https://gitlab.isc.org/tomek/git-snitch

2. Wikipedia article about working week, https://en.wikipedia.org/wiki/Workweek_and_weekend#Length

3.